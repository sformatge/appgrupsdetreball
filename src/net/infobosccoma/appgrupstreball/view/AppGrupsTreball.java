package net.infobosccoma.appgrupstreball.view;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import net.infobosccoma.appgrupstreball.model.Estudiant;
import net.infobosccoma.appgrupstreball.model.GrupTreball;

/**
 * Classe que conté el mètode principal de l'aplicació de gestió d'estudiants i
 * grups de treball.
 *
 * L'aplicació ha d gestionar, mitjançant una BDOO db4o, a quins grups de
 * treball pertanyen diferents estudiants dins una escola. Donat un grup de
 * treball, aquest pot tenir assignats diversos estudiants, però tot estudiant
 * només té un (però sempre un) únic grup de treball. L’aplicació ha de poder
 * fer el següent: - Donar d’alta un nou estudiant. A l’hora d’assignar-li un
 * grup, si el nom indicat no existeix, es crea un nou grup. Si existeix, a
 * l’estudiant se li assigna aquell grup. - Reassignar un estudiant a un altre
 * grup de treball. Aquest grup ja ha d’existir. Si, en fer-ho, el grup antic
 * queda sense membres, cal esborrar-lo de la BDOO. - Llistar tots els grups
 * existents. - Llistar tots els estudiants (i a quin grup pertanyen).
 *
 * Es considera que els noms dels grups i dels estudiants són únics al sistema.
 * No hi pot haver noms repetits. En base a la descripció, també cal remarcar
 * que l’única manera de crear grups nous és afegint-hi nous estudiants.
 *
 * @author ?????????
 */
public class AppGrupsTreball {

    static ObjectContainer db;

    public static void main(String[] args) {
        db = Db4oEmbedded.openFile("laMevaBD.db4o");
        //  System.out.println(crearEstudiant("joan", "grup1").getGrupTreball());

        int seleccio;
        Scanner lector = new Scanner(System.in);

        System.out.println("--------APP GRUPS DE TREBALL------");
        System.out.println("----------------------------------\n");
        System.out.println("1 - Afegir Estudiant");
        System.out.println("2 - Modificar grup estudiant");
        System.out.println("3 - Llistar estudiants");
        System.out.println("4 - Llistar grups");
        System.out.println("5 - Quit");

        seleccio = lector.nextInt();
        switch (seleccio) {
            case 1:
                System.out.println("Nom: ");
                String nom = lector.next();
                System.out.println("Grup: ");
                String grup = lector.next();
                crearEstudiant(nom, grup);
                break;  //optional  
            case 2:
                System.out.println("Nom: ");
                String nomnou = lector.next();
                System.out.println("Grup: ");
                String grupnou = lector.next();
                modificarGrupEstudiant(nomnou, grupnou);
                break;  //optional  ¨
            case 3:
                llistarEstudiants();
                break;  //optional  
            case 4:
                llistarGrups();
                break;  //optional  

        }

        db.close();

    }

    static void crearEstudiant(String nom, String grupNouEstudiant) {

        Estudiant estudiantNou = null;

        GrupTreball grup = new GrupTreball(grupNouEstudiant, null);
        ObjectSet<GrupTreball> result = db.queryByExample(grup);
        if (result.size() >= 1) {
            grup = result.next();
        }
        estudiantNou = new Estudiant(nom, grup);
        grup.sumaEstudiant();
        db.store(estudiantNou);

        //return estudiantNou;
    }

    static void modificarGrupEstudiant(String nom, String grupNou) {
        GrupTreball grup = new GrupTreball(grupNou, null);
        Estudiant estudiant = new Estudiant(nom, grup);
        
        estudiant.reassignaGrup(grup);
        db.store(estudiant);

    }

    static void llistarEstudiants() {
        List<Estudiant> all = db.query(Estudiant.class);

        Iterator<Estudiant> it = all.iterator();
        while (it.hasNext()) {
            Estudiant hey = it.next();
            System.out.println(hey);
            //db.delete(hey);
        }
    }

    static void llistarGrups() {
        List<GrupTreball> all = db.query(GrupTreball.class);

        Iterator<GrupTreball> it = all.iterator();
        while (it.hasNext()) {
            GrupTreball hey = it.next();
            System.out.println(it.next());
            //db.delete(hey);
        }
    }
}
